/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

pub mod device
{
    use core::fmt;

    pub(crate) struct DeviceIdentificationData
    {
        pub(crate) name: String,
        pub(crate) id: Vec<u8>,
    }

    impl DeviceIdentificationData {
        pub(crate) fn new(new_name: String, address: Vec<u8>) -> DeviceIdentificationData
        {
            DeviceIdentificationData { name: new_name, id: address }
        }
    }

    pub(crate) fn address_represented_as_hex(data: &Vec<u8>) -> String {
        let fragments: Vec<String> = data.iter().map(|&value| format!("{:02X}", value)).collect();
        fragments.as_slice().join(":")
    }

    impl fmt::Display for DeviceIdentificationData
    {
        fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
            let output = format!("{}: {}", self.name, address_represented_as_hex(&self.id));
            write!(formatter, "{}", output)
        }
    }

    pub(crate) trait Operations
    {
        fn is_equal(&self, other: &Self) -> bool;
    }

    impl Operations for DeviceIdentificationData
    {
        fn is_equal(&self, other: &Self) -> bool
        {
            return self.name == other.name && self.id == other.id;
        }
    }
}
