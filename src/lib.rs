/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

use std::time::{Duration, Instant};
use std::thread::sleep;
use std::io::{Write, Read};

use mio::{Events, Poll, Ready, PollOpt, Token};

mod device;
use crate::device::device::{DeviceIdentificationData, Operations};

use logger::log;

mod communication_medium;
use crate::communication_medium::communication_channel;
use crate::communication_medium::communication_channel::{Socket, Device};

use data_formats::session_key::SessionKey;
use data_formats::authentication_message::AuthenticationMessage;
use data_formats::message_separator::{MessageSeparator};
use data_formats::{generic_message, Validation, Markers};
use data_formats::generic_message::{MessageOperations};
use data_formats::alive_message::AliveMessage;


pub fn start_acquisition(
    device_name: String,
    device_address: Vec<u8>,
    device_max_timeout: u64,
    process_message: std::sync::mpsc::Sender<Vec<u8>>,
    monitoring_ended: mio_extras::channel::Receiver<bool>)
{

    let desired_device = DeviceIdentificationData::new(device_name, device_address);
    let device_timeout= Duration::from_secs(device_max_timeout);
    let session_key = SessionKey::new();

    let socket = loop {
        let authentication_message = AuthenticationMessage::new_with_key(session_key.clone());

        log::trace(format!("Looking for device {}", desired_device));
        let device = get_desired_device(&desired_device);

        log::trace(format!("Attempting connection with {}", device));
        let mut socket = match communication_channel::get_connection_handle(&device.id) {
            Ok(socket) => socket,
            Err(error) =>
                {
                    log::error(format!("Error while connecting to device {} : {}", device, error));
                    continue;
                }
        };

        log::trace(format!("Send authorization request to {}", device));
        match socket.connection.write_all(&authentication_message) {
            Ok(_) => {},
            Err(error) => {
                log::error(format!("Error while sending authentication token to device {} : {}", device, error));
                continue;
            }
        };

        break socket;
    };

    log::trace(format!("Listening for data from device"));
    handle_incoming_data(device_timeout, socket, session_key, process_message, monitoring_ended);
}

fn get_desired_device(desired_device: &DeviceIdentificationData) -> DeviceIdentificationData {
    //TODO get this from a config file
    let duration_between_device_scans = Duration::from_secs(10);
    return loop {
        match communication_channel::get_visible_devices(None) {
            Ok(visible_devices) => {
                match visible_devices
                    .iter()
                    .map(|device| DeviceIdentificationData::from_bluetooth_device(device))
                    .find(|proposed_device| proposed_device.is_equal(&desired_device))
                    {
                        Some(device) => break device,
                        None => {}
                    };
            },
            Err(error) => {
                log::error(format!("Error while scanning for BT devices: {}. Next retry is in {}[s].", error, duration_between_device_scans.as_secs()));
                sleep(duration_between_device_scans);
            }
        };
    };
}

trait Conversions
{
    fn from_bluetooth_device(device: &Device ) -> Self;
}

impl Conversions for DeviceIdentificationData
{
    fn from_bluetooth_device(device: &Device) -> Self
    {
        return DeviceIdentificationData::new(device.name(), device.address());
    }
}

//TODO move to event handling crate
fn handle_incoming_data(
    device_timeout: Duration,
    mut socket: Socket,
    session_key: Vec<u8>,
    process_message: std::sync::mpsc::Sender<Vec<u8>>,
    monitoring_ended: mio_extras::channel::Receiver<bool>)
{

    let listening_start_timestamp = Instant::now();
    let mut events = Events::with_capacity(1024);
    let poll =  match initiate_poll(&socket, &monitoring_ended) {
        Ok(new_poll) => new_poll,
        Err(error) =>  { log::error(error); return; }
    };
    let mut message = generic_message::GenericMessage { content: vec![] };
    let mut event_timeout = Some(device_timeout);
    #[cfg(debug_assertions)] {
        event_timeout = None;
    }

    'event_loop: loop {
        match wait_for_event(device_timeout, &mut events, &poll, event_timeout) {
            Ok(_) => {},
            Err(error) => { log::error(error); break; }
        };

        for event in &events {
            if event.token() == Token(0)
            {
                if !event.readiness().is_readable() { log::trace(format!("Channel closed by the device.")); return; }
                let data_segment = match get_data_segment(&mut socket) {
                    Ok(data) => data,
                    Err(error) => { log::error(error); break; }
                };

                message.add(data_segment);
                if MessageSeparator::is_valid_from(vec![data_segment]) {
                    #[cfg(debug_assertions)] {
                        log::debug(format!("Received message {:?}",message.content));
                    }

                    if event_timeout == None {
                        //TODO strip data from protocol flags
                        match process_message.send(message.content.clone()) {
                            Ok(_) => {},
                            Err(error) => {
                                log::error(format!("Error while sending message. {}",error));
                                break 'event_loop;
                            }
                        };
                    } else if AliveMessage::is_valid_from(message.content, &session_key) {
                        match process_message.send(AliveMessage::get_start_symbol().clone()) {
                            Ok(_) => {},
                            Err(error) => {
                                log::error(format!("Error while sending message. {}",error));
                                break 'event_loop;
                            }
                        };
                        event_timeout = None;
                    }
                    message = generic_message::GenericMessage { content: vec![] };
                }

                break;
            } else if event.token() == Token(1) {
                log::trace(format!("Stop listening for incoming data from device."));
                break 'event_loop;
            }
        }

        event_timeout = match update_event_timeout(device_timeout, listening_start_timestamp, event_timeout) {
            Ok(new_value) => new_value,
            Err(error) => { log::error(error); break; }
        };

        match poll.reregister(&socket.connection, Token(0), Ready::readable(), PollOpt::level() | PollOpt::oneshot()) {
            Ok(_) => {},
            Err(error) => { log::error(format!("Error while registering the device connection with the event listener. {}",error)); break;  }
        };
    }
}

//TODO move to event handling crate
fn initiate_poll(socket: &Socket, monitoring_ended: &mio_extras::channel::Receiver<bool>) -> Result<Poll, String> {
    let poll = match Poll::new() {
        Ok(poll) => poll,
        Err(error) => {
            return Err(format!("Error while creating an event listener {}", error));
        }
    };
    match poll.register(&socket.connection, Token(0), Ready::readable(), PollOpt::level() | PollOpt::oneshot()) {
        Ok(_) => {},
        Err(error) => {
            return Err(format!("Error while registering the device connection with the event listener. {}", error));
        }
    };
    match poll.register(monitoring_ended, Token(1), Ready::readable(), PollOpt::edge()) {
        Ok(_) => {},
        Err(error) => {
            return Err(format!("Error while registering the status_receiver channel with the event listener. {}", error));
        }
    };
    Ok(poll)
}

//TODO move to event handling crate
fn wait_for_event(device_timeout: Duration, events: &mut Events, poll: &Poll, event_timeout: Option<Duration>) -> Result<(), String>{

    match poll.poll(events, event_timeout) {
        Ok(number_of_events) => {
            if 0 == number_of_events {
                return Err(format!("Device did not send the alive message within {} ", device_timeout.as_secs()));
            }
        },
        Err(error) => {
            return Err(format!("Error while waiting for data from device {}", error));
        }
    };
    Ok(())
}

//TODO move to event handling crate
fn get_data_segment(socket: &mut Socket) -> Result<u8, String> {

    let mut received_data = [0; 1];

    match socket.connection.read_exact(&mut received_data) {
        Ok(_) => {}
        Err(error) => {
            return Err( format!("Error while reading data from device. {}", error) );
        }
    };

    let &data_segment = match received_data.first() {
        Some(data) => data,
        None => {
            return Err(format!("Invalid data received from device"));
        }
    };

    Ok(data_segment)
}

//TODO move to event handling crate
fn update_event_timeout(device_timeout: Duration, authorization_message_timestamp: Instant, event_timeout: Option<Duration>) -> Result<Option<Duration>, String>{
    if event_timeout == None {
        return Ok(None);
    }

    if authorization_message_timestamp.elapsed() + Duration::from_secs(1) >= device_timeout {
        return Err(format!("Device did not send the alive message within {} ", device_timeout.as_secs()));
    }

    Ok(Some(device_timeout - authorization_message_timestamp.elapsed()))
}
