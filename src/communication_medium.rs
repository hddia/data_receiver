/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

pub mod communication_channel
{
    use bluetooth_serial_port::{BtProtocol, BtSocket, BtDevice, BtAddr};
    use std::time;
    use crate::device::device::address_represented_as_hex;

    pub(crate) struct Device {
        pub(crate) instance: BtDevice
    }

    impl Device {
        pub(crate) fn name(&self) -> String {
            self.instance.name.to_string()
        }

        pub(crate) fn address(&self) -> Vec<u8> {
            self.instance.addr.0.to_vec()
        }
    }

    pub(crate) fn get_visible_devices(timeout: Option<u64>) -> Result<Vec<Device>, String>
    {
        let (timeout_in_seconds, timeout_is_set)= match timeout
        {
            Some(value) => (value, true),
            None => (10, false)
        };

        let scan_timeout = time::Duration::from_secs(timeout_in_seconds);

        loop
        {
            let devices: Vec<BtDevice> = match bluetooth_serial_port::scan_devices(scan_timeout)
            {
                Ok(found_devices) => {
                    found_devices
                },
                Err(error) => return Err(error.to_string())
            };

            if timeout_is_set || 0 < devices.len()
            {
                let mut result: Vec<Device> = vec![];
                for bt_device in devices {
                    result.push( Device { instance: bt_device} );
                }
                return Ok(result);
            }
        }
    }

    pub(crate) struct Socket {
        pub(crate) connection: BtSocket
    }

    pub(crate) fn get_connection_handle(address: &Vec<u8>) -> Result<Socket, String>
    {
        let mut socket = match BtSocket::new(BtProtocol::RFCOMM)
        {
            Ok(socket) => socket,
            Err(error) => {
                return Err(format!("Error while opening RFCOMM socket to: {}, {}", address_represented_as_hex(address), error))
            }
        };

        let mut device_address = BtAddr::any();
        let address_length = device_address.0.len();
        device_address.0.copy_from_slice(&address[..address_length]);
        match socket.connect(device_address)
        {
            Ok(_) => return Ok( Socket { connection: socket} ),
            Err(error) => return Err(format!("Error while connecting to: {}, {}", address_represented_as_hex(address), error))
        };
    }
}



